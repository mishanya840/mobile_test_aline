import 'dart:async';

import 'package:adhara_socket_io/adhara_socket_io.dart';
//http://192.168.1.47:19002/
class Repository {

  Repository(this.host, this.port);

  final String host;
  final num port;
  SocketIO _socket;
  SocketIOManager _socketIOManager = SocketIOManager();

  Future<SocketIO> _getSocket() async {
    if (_socket != null) {
      _socketIOManager.clearInstance(_socket);
    }
    _socket = await _socketIOManager.createInstance('http://$host:$port/');
    return _socket;
  }

  void connect({SocketEventListener onSportList, SocketEventListener onSportChildren}) async {
    (await _getSocket())
      ..on('sport-list', onSportList)
      ..on('get-sport', onSportChildren)
      ..connect();
  }

  void emitGetSport(num id) {
    _socket.emit('get-sport', [{'id': id}]);
  }
}
