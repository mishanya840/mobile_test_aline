import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:mobile_test_aline/models/models.dart';

@immutable
abstract class ListState extends Equatable {
  ListState([List props = const []]) : super(props);
}

class Loading extends ListState {
  @override
  String toString() => 'Loading';
}

class Loaded extends ListState {
  final List<Item> items;

  Loaded({@required this.items}) : super([items]);

  @override
  String toString() => 'Loaded { items: ${items.length} }';
}

class Failure extends ListState {
  final String errorMessage;

  Failure({this.errorMessage}) : super([errorMessage]);

  @override
  String toString() => 'Failure';
}
