import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobile_test_aline/bloc/bloc.dart';
import 'package:mobile_test_aline/models/models.dart';
import 'package:mobile_test_aline/repository.dart';

class ListBloc extends Bloc<ListEvent, ListState> {
  final Repository repository;

  ListBloc({@required this.repository});

  @override
  ListState get initialState => Loading();

  @override
  Stream<ListState> mapEventToState(
    ListEvent event,
  ) async* {
    if (event is Connect) {
      try {
        repository.connect(
            onSportList: (data) {
              List<Item> items = List<Map<String, dynamic>>.from(data)
                  .map((item) => Item(id: item['id'], name: item['name']))
                  .toList();
              dispatch(NewSportItems(items: items));
            },
            onSportChildren: (data) {
              dispatch(ChildrenLoaded(
                  id: Map.from(data)['meta']['id'],
                  children: List<String>.from(Map.from(data)['response'])));
            }
        );
      } catch (e) {
        print('ERROR: $e');
        yield Failure();
      }
    }
    if (event is NewSportItems) {
      final listState = currentState;
      if (listState is Loading) {
        yield Loaded(items: event.items);
      }
    }
    if (event is FetchChildren) {
      final listState = currentState;
      if (listState is Loaded) {
        repository.emitGetSport(event.id);
      }
    }
    if (event is ChildrenLoaded) {
      final listState = currentState;
      if (listState is Loaded) {
        final List<Item> updatedItems = List<Item>.from(listState.items)
            .map((Item item) => item.id == event.id
                ? item.copyWith(children: event.children)
                : item)
            .toList();
        yield Loaded(items: updatedItems);
      }
    }
    if (event is ExpansionOpen) {
      final listState = currentState;
      if (listState is Loaded) {
        final List<Item> updatedItems = List<Item>.from(listState.items)
            .map((Item item) => item.copyWith(isExpanded: item.id == event.id))
            .toList();
        yield Loaded(items: updatedItems);
      }
    }
  }
}
