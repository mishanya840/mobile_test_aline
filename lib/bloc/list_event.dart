import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:mobile_test_aline/models/models.dart';

@immutable
abstract class ListEvent extends Equatable {
  ListEvent([List props = const []]) : super(props);
}

class Connect extends ListEvent {
  @override
  String toString() => 'Connect';
}
class NewSportItems extends ListEvent {
  final List<Item> items;

  NewSportItems({@required this.items}) : super([items]);

  @override
  String toString() => 'NewSportItems';
}

class FetchChildren extends ListEvent {
  final num id;

  FetchChildren({@required this.id}) : super([id]);

  @override
  String toString() => 'FetchChildren { id: $id }';
}

class ExpansionOpen extends ListEvent {
  final num id;

  ExpansionOpen({@required this.id}) : super([id]);

  @override
  String toString() => 'ExpansionOpen { id: $id }';
}

class ChildrenLoaded extends ListEvent {
  final num id;
  final List<String> children;

  ChildrenLoaded({@required this.id, @required this.children}) : super([id, children]);

  @override
  String toString() => 'ChildrenLoaded { id: $id, children $children }';
}
