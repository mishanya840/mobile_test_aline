import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobile_test_aline/bloc/bloc.dart';
import 'package:mobile_test_aline/models/models.dart';
import 'package:mobile_test_aline/repository.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(StartPage());
}

class StartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sport App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Socket URL'),
        ),
        body: MyCustomForm()
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() => MyCustomFormState();
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var hostController = TextEditingController();
    var portController = TextEditingController();
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                controller: hostController,
                decoration: InputDecoration(
                  labelText: 'Host',
                  hintText: 'ex: 192.168.1.47'
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Enter some host';
                  }
                  if (!RegExp(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$").hasMatch(value)) {
                    return 'Not valid format';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: portController,
                decoration: InputDecoration(
                  labelText: 'Port',
                  hintText: 'ex: 19002'
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Enter some port';
                  }
                  if (int.tryParse(value) == null) {
                    return 'Not numeric port';
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomePage(
                            host: hostController.text,
                            port: int.parse(portController.text),
                          )));
                    }
                  },
                  child: Text('Connect'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class HomePage extends StatelessWidget {

  HomePage({this.host, this.port});

  final String host;
  final num port;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sport events'),
      ),
      body: BlocProvider(
        builder: (context) =>
            ListBloc(repository: Repository(host, port))..dispatch(Connect()),
        child: SportItemList(),
      ),
    );
  }
}

class SportItemList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final listBloc = BlocProvider.of<ListBloc>(context);
    return BlocBuilder(
      bloc: listBloc,
      builder: (BuildContext context, ListState state) {
        if (state is Loading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is Failure) {
          return Center(
            child: Text('Oops something went wrong!\n${state.errorMessage}', maxLines: 2),
          );
        }
        if (state is Loaded) {
          if (state.items.isEmpty) {
            return Center(
              child: Text('no content'),
            );
          }
          return ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              var item = state.items[index];
              return ItemTile(
                item: item,
                onExpansionOpen: (id) {
                  listBloc.dispatch(ExpansionOpen(id: id));
                  if (item.children == null) {
                    listBloc.dispatch(FetchChildren(id: id));
                  }
                },
              );
            },
            itemCount: state.items.length,
          );
        }
        return Center(
          child: Text('Oops something went wrong!'),
        );
      },
    );
  }
}

class ItemTile extends StatelessWidget {
  final Item item;
  final Function(num) onExpansionOpen;

  const ItemTile({
    Key key,
    @required this.item,
    @required this.onExpansionOpen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      key: ValueKey(item),
      children: item.children?.map((child) => ListTile(title: Text(child)))?.toList() ?? [ListTile(title: Center(child: CircularProgressIndicator()))],
      leading: Text('#${item.id}'),
      title: Text(item.name),
      initiallyExpanded: item.isExpanded,
      onExpansionChanged: (value) {
        if (value) {
          onExpansionOpen(item.id);
        }
      },
    );
  }
}

