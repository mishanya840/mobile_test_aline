import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Item extends Equatable {
  final num id;
  final String name;
  final bool isExpanded;
  final List<String> children;

  Item({
    @required this.id,
    @required this.name,
    this.isExpanded = false,
    this.children,
  }) : super([id, name, children, isExpanded]);

  Item copyWith({
    num id,
    String name,
    bool isExpanded,
    List<String> children,
  }) {
    return Item(
      id: id ?? this.id,
      name: name ?? this.name,
      isExpanded: isExpanded ?? this.isExpanded,
      children: children ?? this.children,
    );
  }

  @override
  String toString() =>
      'Item { id: $id, name: $name, isExpanded: $isExpanded, children: $children }';
}
